﻿-- Домашнее задание №17
USE shop;

-- Добавим два новых заказа Петру
INSERT INTO order_products (order_id, product_id, `count`) VALUES(2, 3, 2);
INSERT INTO order_products (order_id, product_id, `count`) VALUES(2, 4, 3);

-- Посмотрим как у нас преобразовалась суммарная таблица, которая
-- отражает полную информацию о заказе
SELECT * FROM `order`
 INNER JOIN order_products ON order_products.order_id = `order`.id
 INNER JOIN product ON product.id = order_products.product_id;
-- Получим суммарную стоимость заказов Василия по его id
SELECT * FROM `order`
 INNER JOIN order_products ON order_products.order_id = `order`.id
 INNER JOIN product ON product.id = order_products.product_id
  WHERE `order`.id = 1;

-- Получим суммарную стоимость каждого заказа Василия
SELECT *, price*`count` AS total_price FROM `order`
 INNER JOIN order_products ON order_products.order_id = `order`.id
 INNER JOIN product ON product.id = order_products.product_id
  WHERE `order`.id = 1;

-- Получим суммарную стоимость всех заказов Василия
SELECT SUM(price * `count`) AS total_price FROM `order`
 INNER JOIN order_products ON order_products.order_id = `order`.id
 INNER JOIN product ON product.id = order_products.product_id
  WHERE `order`.id = 1;

-- Поменяв идентификатор на Петра, получим результат по Петру
SELECT SUM(price * `count`) AS total_price FROM `order`
 INNER JOIN order_products ON order_products.order_id = `order`.id
 INNER JOIN product ON product.id = order_products.product_id
  WHERE `order`.id = 2;
